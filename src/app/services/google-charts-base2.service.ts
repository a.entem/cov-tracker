declare var google: any;


export class GoogleChartsBaseService2 {
  constructor() { 
    google.charts.load('49', {'packages':["vegachart"], 'language': 'fr'});
  }


  protected buildChart(data5: any[], chartFunc: any, options: any) : void {
    var func = (chartFunc, options) => {
      var datatable = new google.visualization.DataTable();
        datatable.addColumn({type: 'string', 'id': 'id'});
        datatable.addColumn({type: 'number', 'id': 'rate'});
        datatable.addRows(data5);
    


      chartFunc().draw(datatable, options);



    };   
    var callback = () => func(chartFunc, options);
    google.charts.setOnLoadCallback(callback);
  }
  
}  