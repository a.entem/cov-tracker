declare var google: any;


export class GoogleChartsBaseService {
  constructor() { 
    google.charts.load('49', {'packages':["vegachart"], 'language': 'fr'});
  }


  protected buildChart(data6: any[], chartFunc: any, options: any) : void {
    var func = (chartFunc, options) => {


      var datatable = new google.visualization.DataTable();
        datatable.addColumn({type: 'string', 'id': 'date'});
        datatable.addColumn({type: 'number', 'id': 'p'});
         datatable.addColumn({type: 'string', 'id': 'cl_age90'});
        datatable.addRows(data6);
   

      chartFunc().draw(datatable, options);



    };   
    var callback = () => func(chartFunc, options);
    google.charts.setOnLoadCallback(callback);
  }
  
}  