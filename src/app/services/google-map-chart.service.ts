import { GoogleChartsBaseService2 } from './google-charts-base2.service';
import { Injectable } from '@angular/core';
import { ChartConfig2 } from './../models/ChartConfig2.model';
declare var google: any;

@Injectable()
export class GoogleMapChartService extends GoogleChartsBaseService2 {

 constructor() { super(); }

 public BuildPieChart(elementId5: string, data5: any[], config5: 
 ChartConfig2) : void {  
 var chartFunc = () => { return new 
 google.visualization.VegaChart(document.getElementById(elementId5)); };


  var options = {
          "vega": 
{
  "$schema": "https://vega.github.io/schema/vega/v5.json",
  "width": 500,
  "height": 500,
  "autosize": "none",
  "title":"blalvd",
  "signals": [
     
       
    {
      "name": "scale", "value": 2000
    },
    {
      "name": "translateX", "value": 200

    },
    {
      "name": "translateY", "value": 2100
    },
    {
      "name": "shape", "value": "line"
    }

    ],

  "data": [
   
    {
      "name": "france",
      "url": "../assets/data/france-departements",
      "format": {"type": "topojson", "feature": "france-departements"},
      "transform": [
        { "type": "lookup", "from": "datatable", "key": "id", "fields": ["properties.code"], "values": ["rate"] },
        { "type": "filter", "expr": "datum.rate != null" }
      ]
    }
  ],

  "projections": [
    {
      "name": "projection",
      "type": "mercator",
      "scale": {"signal": "scale"},
      "translate": [{"signal": "translateX"}, {"signal": "translateY"}]
    }
  ],

  "scales": [


    
    {
      "name": "color",
      "type": "quantize",
      "domain": [0, 100],
      "range": {"scheme": "greenblue", "count": 5},

    }
  ],

  "legends": [


    {
      "fill": "color",
      "orient": "bottom-left",
      "title": "positifs covid / jour.",
      "format": "1"
    }
  ],

  "marks": [
    {
      "type": "shape",
      "from": {"data": "france"},
      "encode": {
        "enter": { "tooltip": {"signal": "datum.properties.nom + ' : ' +  format( datum.rate, '1') + ' positifs covid / jour.'"}},
        "update": { "fill": {"scale": "color", "field": "rate"} },
        "hover": { "fill": {"value": "red"} }
         

      },
      "transform": [
        { "type": "geoshape", "projection": "projection" }
      ]
    }
  ]
}



        };
//         let montableaDechelle = [0,500];
// console.log(data5[1].max());
//         montableaDechelle = data5[0].max();
// options.vega.scales[0].domain= montableaDechelle;
    

   this.buildChart(data5, chartFunc, options);}


   }