import { GoogleChartsBaseService } from './google-charts-base.service';
import { Injectable } from '@angular/core';
import { ChartConfig } from './../models/ChartConfig.model';


declare var google: any;

@Injectable()
export class GoogleChartService extends GoogleChartsBaseService {

 constructor() { super(); }

 public BuildPieChart(elementId6: string, data6: any[], config6: 
 ChartConfig) : void {  
 var chartFunc = () => { return new 
 google.visualization.VegaChart(document.getElementById(elementId6)); };


  var options = {
          "vega": 
{
  "$schema": "https://vega.github.io/schema/vega/v5.json",
  "description": "A basic line chart example.",
  "width": 450,
  "height": 450,
  "padding": 5,

  "signals": [
    {
      "name": "interpolate",
      "value": "linear",
      "bind": {
        "input": "select",
        "options": [
          "basis",
          "cardinal",
          "catmull-rom",
          "linear",
          "monotone",
          "natural",
          "step",
          "step-after",
          "step-before"
        ]
      }
    }
  ],


  "scales": [
    {
      "name": "date",
      "type": "point",
      "range": "width",
      "domain": {"data": "datatable", "field": "date"}
    },
    {
      "name": "p",
      "type": "linear",
      "range": "height",
      "nice": true,
      "zero": true,
      "domain": {"data": "datatable", "field": "p"}
    },
    {
      "name": "color",
      "type": "ordinal",
      "range": "category",
      "domain": {"data": "datatable", "field": "cl_age90"}
    }
  ],

  "axes": [
    {"orient": "bottom", "scale": "date","labelAngle":70,"labelPadding":25,"labelOverlap":true},
    {"orient": "left", "scale": "p"}
  ],

  "marks": [
    {
      "type": "group",
      "from": {
        "facet": {
          "name": "series",
          "data": "datatable",
          "groupby": "cl_age90"
        }
      },
      "marks": [
        {
          "type": "line",
          "from": {"data": "series"},
          "encode": {
            "enter": {
              "x": {"scale": "date", "field": "date"},
              "y": {"scale": "p", "field": "p"},
              "stroke": {"scale": "color", "field": "cl_age90"},
              "strokeWidth": {"value": 3}
            },
            "update": {
              "interpolate": {"signal": "interpolate"},
              "strokeOpacity": {"value": 1}
            },
            "hover": {
              "strokeOpacity": {"value": 0.5}
            }
          }
        }
      ]
    }
  ]
}



        };
//         let montableaDechelle = [0,500];
// console.log(data5[1].max());
//         montableaDechelle = data5[0].max();
// options.vega.scales[0].domain= montableaDechelle;
    

   this.buildChart(data6, chartFunc, options);}


   }