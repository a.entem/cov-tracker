import { TestBed } from '@angular/core/testing';

import { SpfService } from './spf.service';

describe('SpfService', () => {
  let service: SpfService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SpfService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
