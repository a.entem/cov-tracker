import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import { Observable} from 'rxjs';

const options: {
            headers?: HttpHeaders,
            mode?: 'no-cors',
            observe?: 'body',
            params?: HttpParams,
            reportProgress?: boolean,
            responseType: 'json',
            withCredentials?: boolean
        } = {
            responseType: 'json'
        };

@Injectable({
  providedIn: 'root'
})

export class SpfService {

public constructor( private http: HttpClient ) {}
url : string = "https://geodes.santepubliquefrance.fr/GC_indic.php?lang=fr&prodhash=600f99d2&indic=p&dataset=sp_pos_quot&view=map2&filters=cl_age90=";

getSPFdata(age: number, date :number): Observable<any> {  
    return this.http.get<any>(this.url+age+',jour='+date, options);
 }

}
