import { Component, OnInit, Inject, ChangeDetectorRef, HostListener, ElementRef, ViewChild, Input,Output,OnDestroy,ViewEncapsulation } from '@angular/core';
import { ChartConfig2 } from './../models/ChartConfig2.model';
import { ChartConfig } from './../models/ChartConfig.model';
import { GoogleChartComponent } from 'ng2-google-charts'; 
import { SpfService } from '../services/spf.service';
import { MatCalendarCellCssClasses } from '@angular/material/datepicker';
import { FormControl } from '@angular/forms';
import { NativeDateAdapter } from '@angular/material/core';
import { MatDateFormats } from '@angular/material/core';

import { DatePipe } from '@angular/common';

interface Food {
  value: number;
  viewValue: string;
}

export class AppDateAdapter extends NativeDateAdapter {
  format(date: Date, displayFormat: Object): string {
    if (displayFormat === 'input') {
      let day: string = date.getDate().toString();
      day = +day < 10 ? '0' + day : day;
      let month: string = (date.getMonth() + 1).toString();
      month = +month < 10 ? '0' + month : month;
      let year = date.getFullYear();
      return `${year}-${month}-${day}`;
    }
    return date.toDateString();
  }
}

export const APP_DATE_FORMATS: MatDateFormats = {
  parse: {
    dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
  },
  display: {
    dateInput: 'input',
    monthYearLabel: { year: 'numeric', month: 'numeric' },
    dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric'
    },
    monthYearA11yLabel: { year: 'numeric', month: 'long' },
  }
};


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  providers: [
 
  ],
  encapsulation: ViewEncapsulation.None
})

export class MapComponent implements OnInit {

private subscription :any;

foods: Food[] = [
    {value: 0, viewValue: 'Tout âge'},
    {value: 9, viewValue: '0-10 ans'},
    {value: 19, viewValue: '10-20 ans'},
    {value: 29, viewValue: '20-30 ans'},
    {value: 39, viewValue: '30-40 ans'},
    {value: 49, viewValue: '40-50 ans'},
    {value: 59, viewValue: '50-60 ans'},
    {value: 69, viewValue: '60-70 ans'},
    {value: 79, viewValue: '70-80 ans'},
    {value: 89, viewValue: '80-90 ans'},
    {value: 90, viewValue: '+ de 90ans'}
      ];

  constructor(public spfService: SpfService,private datePipe : DatePipe) { }

  data6=[];
  data5 = [];
  config5: ChartConfig2;
  elementId5: string; 
  config6: ChartConfig;
  elementId6: string; 
  values;
  values2;
  age2;
  date2 = Date.now();
  date4;
  showSpinner: boolean;
  minDate = new Date(2020, 4, 13); //minDate is 1st Jan 2019
  maxDate = new Date( Date.now()); //maxDate is 1st Jan 2020
  date = new FormControl(new Date(Date.now()));
  age = new FormControl(0);


  ngOnInit(): void {
    this.showSpinner = true;
    this.subscription = this.spfService.getSPFdata(this.age2, this.date2).subscribe(reponse => {
        this.values = reponse.content.distribution.values;
        this.values2 =reponse.content.zonrefs[0].values;
        for (let i = 0; i < this.values2.length; i++) {
          let data= [this.values2[i].jour,this.values2[i].p,this.values2[i].cl_age90]
          this.data6.push(data);
        }

        for (let i = 0; i < this.values.length; i++) {
          let data= [i.toString(),this.values[i]]
          this.data5.push(data);
        }
    });
    this.config5 = new ChartConfig2(0,0);
    this.elementId5 = 'myGanttChart5'; 
    this.config6 = new ChartConfig('date',0,0);
    this.elementId6 = 'myGanttChart6';
    setInterval(() => {this.showSpinner = false; }, 2000);
  }


  onSubmitForm(e){
    this.showSpinner=true;
    this.subscription.unsubscribe();
    this.data5=[];
    this.data6=[];
    this.age2=0;
    this.age2=this.age.value;
    this.date4=this.datePipe.transform(this.date.value, 'yyyy-MM-dd');
    this.spfService.getSPFdata(this.age2, this.date4).subscribe(reponse => {
        this.values = reponse.content.distribution.values;
        this.values2 =reponse.content.zonrefs[0].values;
        this.values2 = this.values2.filter(a =>a.cl_age90==this.age2) ;
        this.values2 = this.values2.filter(a =>new Date(a.jour).getTime()<=this.date.value.getTime()) ;

        for (let i = 0; i < this.values.length; i++) {
         let data= [i.toString(),this.values[i]]
         this.data5.push(data);
        }

        for (let i = 0; i < this.values2.length; i++) {
         let data= [this.values2[i].jour,this.values2[i].p,this.values2[i].cl_age90]
         this.data6.push(data);
        }
    });
    setInterval(() => {this.showSpinner = false; }, 2000);
  }

}  