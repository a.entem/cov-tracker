import { Component, OnInit, Inject, ChangeDetectorRef, HostListener, ElementRef, ViewChild, Input,Output,OnDestroy, OnChanges } from '@angular/core';
import { GoogleMapChartService } from './../../services/google-map-chart.service';
import { ChartConfig2 } from './../../models/ChartConfig2.model';
declare var google: any;
import {GoogleChartComponent} from 'ng2-google-charts';

@Component({
  selector: 'app-map-init',
  templateUrl: './map-init.component.html',
  styleUrls: ['./map-init.component.scss']
})
export class MapInitComponent implements OnInit {

 @Input() data5: any[];
    @Input() config5: ChartConfig2;
    @Input() elementId5: string;

 constructor(private _ChartService: GoogleMapChartService) {}

@HostListener('window:resize', ['$event']) onWindowResize(event: any) {
        this.drawChart();  }

ngOnInit(): void {this.drawChart();}
ngOnChanges(): void {this.drawChart();}
drawChart(): void { 
        this._ChartService.BuildPieChart(this.elementId5, this.data5, this.config5);

    }   
}




