import { Component, OnInit, Inject, ChangeDetectorRef, HostListener, ElementRef, ViewChild, Input,Output,OnDestroy, OnChanges } from '@angular/core';
import { GoogleChartService } from './../../services/google-chart.service';
import { ChartConfig } from './../../models/ChartConfig.model';
declare var google: any;
import {GoogleChartComponent} from 'ng2-google-charts';

@Component({
  selector: 'app-graph-init',
  templateUrl: './graph-init.component.html',
  styleUrls: ['./graph-init.component.scss']
})
export class GraphInitComponent implements OnInit {


 @Input() data6: any[];
    @Input() config6: ChartConfig;
    @Input() elementId6: string;

 constructor(private _ChartService: GoogleChartService) {}

@HostListener('window:resize', ['$event']) onWindowResize(event: any) {
        this.drawChart();  }


ngOnInit(): void {this.drawChart();}
ngOnChanges(): void {this.drawChart();}
drawChart(): void { 
        this._ChartService.BuildPieChart(this.elementId6, this.data6, this.config6);

    }   
}
