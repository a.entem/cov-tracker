import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphInitComponent } from './graph-init.component';

describe('GraphInitComponent', () => {
  let component: GraphInitComponent;
  let fixture: ComponentFixture<GraphInitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphInitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphInitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
